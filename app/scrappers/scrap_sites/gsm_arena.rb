module ScrapSites
  class GSMArena
    attr_accessor :res

    def initialize(url, query)
      @@query_param = query
      ScrapSite.build(url) do
        search @@query_param, 'search', 'term'
        open_result :div, html_attrs: '.s_listing div.s_block_4.s_block_4_s115.s_fst.clearfix div.quicklookdiv', link_nr: 1
        @@result = get_element 'div.s_specs_box.s_box_4:nth-of-type(1) ul li:nth-of-type(3) ul li'
        results << @@result
      end
      res << @@result
    end

    def res
      @res ||= []
    end
  end
end