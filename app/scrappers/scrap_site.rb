class ScrapSite
  attr_accessor :url, :page, :result_link

  def self.build(url, &block)
    raise 'Must pass a block' unless block_given?
    ScrapSite.new(url, &block)
  end

  def initialize(url, &block)
    @url = url
    instance_eval &block
  end


  def search(query, rel_path, query_term)
    remote_url = "#{url}#{rel_path}?#{query_term}=#{query}"
    @page = Nokogiri::HTML(open(remote_url))
  end

  def open_result(tag, options={})
    html_attr = options[:html_attrs]
    links = page.css("#{tag}#{html_attr} a").to_a
    if options[:link_nr] && options[:link_nr].is_a?(Integer)
      @result_link = links[options[:link_nr] - 1]['href']
    end
  end

  def get_element(css_element, &block)
    remote_url = "#{url}#{result_link}"
    page = Nokogiri::HTML(open(remote_url))
    res_text = page.at_css(css_element).text
    res_text
  end

  def results
    @results ||= []
  end
end