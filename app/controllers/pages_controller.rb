class PagesController < ApplicationController
  def index
    @result = []
    if params[:query]
      query = params[:query].to_s
      @result << ScrapSites::GSMArena.new('http://www.phonearena.com/', query).res
      respond_to do |format|
        format.js
      end
    end
    @result = @result.join(',')
  end
end
